(ns roadrunner.core-test
  (:require [clojure.test :refer :all]
            [roadrunner.core :refer :all]))

(deftest plus-one-test
  (testing "+ 2"
    (is (= 3 (plus-one 2))))
  (testing "+ 1"
    (is (= 2 (plus-one 1)))))

(deftest second-test
  (testing "foo bar"
    (is (= 3 3))))
