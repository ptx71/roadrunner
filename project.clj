(defproject roadrunner "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "BSD-3-Clause"
            :url "https://opensource.org/license/bsd-3-clause/"}
  :dependencies [[org.clojure/clojure "1.11.1"]]

  :profiles {:test {:plugins [[lein-test-report-junit-xml "0.2.0"]]}}

  :repl-options {:init-ns roadrunner.core})
