(ns roadrunner.core)

(defn plus-one
  "Takes x, returns +1. Used to validate testing."
  [x]
  (+ 1 x))
